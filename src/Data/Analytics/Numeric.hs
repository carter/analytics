--------------------------------------------------------------------
-- |
-- Copyright :  (c) Edward Kmett 2013
-- License   :  BSD3
-- Maintainer:  Edward Kmett <ekmett@gmail.com>
-- Stability :  experimental
-- Portability: non-portable
--
--------------------------------------------------------------------
module Data.Analytics.Numeric
  ( module Data.Analytics.Numeric.Compensated
  , module Data.Analytics.Numeric.Log
  , module Data.Analytics.Numeric.Tropical
  ) where

import Data.Analytics.Numeric.Compensated
import Data.Analytics.Numeric.Log
import Data.Analytics.Numeric.Tropical
