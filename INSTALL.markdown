Installation Instructions
=========================

When building from source you'll need to set up `autoconf`:

* `aclocal`
* `autoreconf -i`

Once that is done you can run `cabal`:

* `cabal install`
